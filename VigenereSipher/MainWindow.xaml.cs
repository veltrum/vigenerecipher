﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VigenereCipher
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private VigenereCipherUtilites cipher;

        public MainWindow()
        {
            cipher = new VigenereCipherUtilites();
            InitializeComponent();
        }

        private void encryptButton_Click(object sender, RoutedEventArgs e)
        {
            cipher.MessageText = messageBox.Text;
            cipher.CipherKey = keyBox.Text;

            String cipherMessage = cipher.EncryptCipher();
            MessageBox.Show(this, cipherMessage);
        }

        private void decrpytButton_Click(object sender, RoutedEventArgs e)
        {
            cipher.MessageText = messageBox.Text;
            cipher.CipherKey = keyBox.Text;

            String cipherMessage = cipher.DecrpyCipher();
            MessageBox.Show(this, cipherMessage);
        }

        private void messageBox_GotFocus(object sender, RoutedEventArgs e)
        {
            messageBox.Text = "";
        }

        private void keyBox_GotFocus(object sender, RoutedEventArgs e)
        {
            keyBox.Text = "";
        }
    }
}
