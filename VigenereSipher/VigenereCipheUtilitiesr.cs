﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VigenereCipher
{
    class VigenereCipherUtilites
    {
        private String messageText = "";
        private String cipherKey = "";

        public Dictionary<String, int> LettertoNumber { get; set; }
        public Dictionary<int, String> NumbertoLetter { get; set; }
        public String[,] VigenereSquare { get; set; }

        public String CipherKey
        {
            get
            {
                return cipherKey;
            }
            set
            {
                cipherKey = value;
                cipherKey = cipherKey.Replace(" ", "");
                cipherKey = cipherKey.ToUpper();
            }
        }

        public String MessageText
        {
            get
            {
                return messageText;
            }
            set
            {
                // Remove spaces, and capitalize message
                messageText = value;
                messageText = messageText.Replace(" ", "");
                messageText = messageText.ToUpper();
            }
        }
        public String CipherText { get; set; }

        public VigenereCipherUtilites()
        {
            CipherKey = "";
            MessageText = "";
            CipherText = "";

            LettertoNumber = new Dictionary<string, int>();
            LettertoNumber.Add("A", 0);
            LettertoNumber.Add("B", 1);
            LettertoNumber.Add("C", 2);
            LettertoNumber.Add("D", 3);
            LettertoNumber.Add("E", 4);
            LettertoNumber.Add("F", 5);
            LettertoNumber.Add("G", 6);
            LettertoNumber.Add("H", 7);
            LettertoNumber.Add("I", 8);
            LettertoNumber.Add("J", 9);
            LettertoNumber.Add("K", 10);
            LettertoNumber.Add("L", 11);
            LettertoNumber.Add("M", 12);
            LettertoNumber.Add("N", 13);
            LettertoNumber.Add("O", 14);
            LettertoNumber.Add("P", 15);
            LettertoNumber.Add("Q", 16);
            LettertoNumber.Add("R", 17);
            LettertoNumber.Add("S", 18);
            LettertoNumber.Add("T", 19);
            LettertoNumber.Add("U", 20);
            LettertoNumber.Add("V", 21);
            LettertoNumber.Add("W", 22);
            LettertoNumber.Add("X", 23);
            LettertoNumber.Add("Y", 24);
            LettertoNumber.Add("Z", 25);

            NumbertoLetter = new Dictionary<int, string>();
            NumbertoLetter.Add(0, "A");
            NumbertoLetter.Add(1, "B");
            NumbertoLetter.Add(2, "C");
            NumbertoLetter.Add(3, "D");
            NumbertoLetter.Add(4, "E");
            NumbertoLetter.Add(5, "F");
            NumbertoLetter.Add(6, "G");
            NumbertoLetter.Add(7, "H");
            NumbertoLetter.Add(8, "I");
            NumbertoLetter.Add(9, "J");
            NumbertoLetter.Add(10, "K");
            NumbertoLetter.Add(11, "L");
            NumbertoLetter.Add(12, "M");
            NumbertoLetter.Add(13, "N");
            NumbertoLetter.Add(14, "O");
            NumbertoLetter.Add(15, "P");
            NumbertoLetter.Add(16, "Q");
            NumbertoLetter.Add(17, "R");
            NumbertoLetter.Add(18, "S");
            NumbertoLetter.Add(19, "T");
            NumbertoLetter.Add(20, "U");
            NumbertoLetter.Add(21, "V");
            NumbertoLetter.Add(22, "W");
            NumbertoLetter.Add(23, "X");
            NumbertoLetter.Add(24, "Y");
            NumbertoLetter.Add(25, "Z");

            VigenereSquare = new String[26, 26] { { "A","B","C","D","E","F","G", "H","I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                                                  { "A","B","C","D","E","F","G", "H","I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                                                  { "A","B","C","D","E","F","G", "H","I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                                                  { "A","B","C","D","E","F","G", "H","I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                                                  { "A","B","C","D","E","F","G", "H","I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                                                  { "A","B","C","D","E","F","G", "H","I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                                                  { "A","B","C","D","E","F","G", "H","I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                                                  { "A","B","C","D","E","F","G", "H","I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                                                  { "A","B","C","D","E","F","G", "H","I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                                                  { "A","B","C","D","E","F","G", "H","I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                                                  { "A","B","C","D","E","F","G", "H","I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                                                  { "A","B","C","D","E","F","G", "H","I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                                                  { "A","B","C","D","E","F","G", "H","I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                                                  { "A","B","C","D","E","F","G", "H","I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                                                  { "A","B","C","D","E","F","G", "H","I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                                                  { "A","B","C","D","E","F","G", "H","I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                                                  { "A","B","C","D","E","F","G", "H","I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                                                  { "A","B","C","D","E","F","G", "H","I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                                                  { "A","B","C","D","E","F","G", "H","I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                                                  { "A","B","C","D","E","F","G", "H","I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                                                  { "A","B","C","D","E","F","G", "H","I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                                                  { "A","B","C","D","E","F","G", "H","I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                                                  { "A","B","C","D","E","F","G", "H","I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                                                  { "A","B","C","D","E","F","G", "H","I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                                                  { "A","B","C","D","E","F","G", "H","I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" },
                                                  { "A","B","C","D","E","F","G", "H","I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" } };
        }

        private String formatKey(int messageLength)
        {
            String formattedKey = CipherKey;

            // The key must be as long as the message
            if (messageText.Count() > formattedKey.Count())
            {
                int keyCharactersToAdd = messageText.Count() - formattedKey.Count();

                while (keyCharactersToAdd > 0)
                {
                    if (keyCharactersToAdd >= CipherKey.Count())
                    {
                        formattedKey += CipherKey;
                        keyCharactersToAdd -= CipherKey.Count();
                    }
                    else
                    {
                        for (int k = 0; k < CipherKey.Count() && 0 < keyCharactersToAdd; ++k)
                        {
                            formattedKey += CipherKey.ElementAt(k);
                            --keyCharactersToAdd;
                        }
                    }
                }
            }

            return formattedKey;
        }

        public String EncryptCipher()
        {
            String EncryptedText = "";
            String formattedKey = formatKey(messageText.Length);

            // Message index
            for(int i = 0; i < messageText.Length; ++i)
            {
                // Get the number for the message character at index i
                int numberFromMessageChar = LettertoNumber[messageText.ElementAt(i).ToString()];

                // Get the number for the key character at index i
                int numberFromKeyChar = LettertoNumber[formattedKey.ElementAt(i).ToString()];

                // Get the cipher character from the message and key. Add it to the cipher text
                EncryptedText += NumbertoLetter[(numberFromMessageChar + numberFromKeyChar) % 26];
            }

            return EncryptedText;
        }

        public String DecrpyCipher()
        {
            String DecryptedText = "";
            String formattedKey = formatKey(messageText.Length);

            for(int i = 0; i < messageText.Length; ++i)
            {
                // Get the number for the message character at index i
                int numberFromMessageChar = LettertoNumber[messageText.ElementAt(i).ToString()];

                // Get the number for the key character at index i
                int numberFromKeyChar = LettertoNumber[formattedKey.ElementAt(i).ToString()];

                // Get the cipher character from the message and key. Add it to the cipher text
                DecryptedText += NumbertoLetter[(numberFromMessageChar - numberFromKeyChar + 26) % 26];
            }

            return DecryptedText;
        }
    }
}
